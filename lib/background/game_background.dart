import 'package:bonfiremodify/base/game_component.dart';
import 'package:bonfiremodify/util/priority_layer.dart';
import 'package:flamemodify/components.dart';

/// Base to create your custom game background
class GameBackground extends GameComponent {
  @override
  int get priority => LayerPriority.BACKGROUND;

  @override
  PositionType get positionType => PositionType.viewport;
}
