import 'package:bonfiremodify/base/game_component.dart';
import 'package:bonfiremodify/collision/object_collision.dart';
import 'package:bonfiremodify/map/tile/tile.dart';
import 'package:bonfiremodify/map/tile/tile_model.dart';
import 'package:bonfiremodify/util/priority_layer.dart';
import 'package:flamemodify/extensions.dart';

abstract class MapGame extends GameComponent {
  List<TileModel> tiles;
  Size? mapSize;
  Vector2? mapStartPosition;
  double tileSizeToUpdate;
  List<Tile> childrenTiles = [];

  MapGame(this.tiles, {this.tileSizeToUpdate = 0});

  Iterable<Tile> getRendered();

  Iterable<ObjectCollision> getCollisionsRendered();
  Iterable<ObjectCollision> getCollisions();

  Future<void> updateTiles(List<TileModel> map);

  Size getMapSize();

  void removeTile(String id);
  Future addTile(TileModel tileModel);

  void setLinePath(List<Offset> linePath, Color color, double strokeWidth) {}

  @override
  int get priority => LayerPriority.MAP;

  void renderDebugMode(Canvas canvas) {
    super.renderDebugMode(canvas);
    for (Tile t in getRendered()) {
      t.renderDebugMode(canvas);
    }
  }

  @override
  bool get isVisible => true;
}
