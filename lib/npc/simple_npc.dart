import 'package:bonfiremodify/npc/npc.dart';
import 'package:bonfiremodify/util/direction.dart';
import 'package:bonfiremodify/util/direction_animations/simple_direction_animation.dart';
import 'package:bonfiremodify/util/mixins/direction_animation.dart';
import 'package:flamemodify/components.dart';

///
/// Created by
///
/// ─▄▀─▄▀
/// ──▀──▀
/// █▀▀▀▀▀█▄
/// █░░░░░█─█
/// ▀▄▄▄▄▄▀▀
///
/// Rafaelbarbosatec
/// on 22/03/22

/// Enemy with animation in all direction
class SimpleNpc extends Npc with DirectionAnimation {
  SimpleNpc({
    required Vector2 position,
    required Vector2 size,
    required SimpleDirectionAnimation animation,
    double speed = 100,
    Direction initDirection = Direction.right,
  }) : super(
          position: position,
          size: size,
          speed: speed,
        ) {
    this.animation = animation;
    lastDirection = initDirection;
    lastDirectionHorizontal =
        initDirection == Direction.left ? Direction.left : Direction.right;
  }
}
