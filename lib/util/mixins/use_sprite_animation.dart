import 'dart:ui';

import 'package:bonfiremodify/base/game_component.dart';
import 'package:bonfiremodify/util/extensions/extensions.dart';
import 'package:flamemodify/components.dart';

///
/// Created by
///
/// ─▄▀─▄▀
/// ──▀──▀
/// █▀▀▀▀▀█▄
/// █░░░░░█─█
/// ▀▄▄▄▄▄▀▀
///
/// Rafaelbarbosatec
/// on 04/02/22
mixin UseSpriteAnimation on GameComponent {
  /// Animation that will be drawn on the screen.
  SpriteAnimation? animation;

  @override
  void render(Canvas canvas) {
    super.render(canvas);
    if (isVisible) {
      animation?.getSprite().renderWithOpacity(
            canvas,
            this.position,
            this.size,
            opacity: opacity,
          );
    }
  }

  @override
  void update(double dt) {
    super.update(dt);
    if (this.isVisible) {
      animation?.update(dt);
    }
  }
}
