import 'dart:ui';

import 'package:bonfiremodify/base/game_component.dart';
import 'package:bonfiremodify/util/extensions/extensions.dart';
import 'package:flamemodify/sprite.dart';

///
/// Created by
///
/// ─▄▀─▄▀
/// ──▀──▀
/// █▀▀▀▀▀█▄
/// █░░░░░█─█
/// ▀▄▄▄▄▄▀▀
///
/// Rafaelbarbosatec
/// on 04/02/22
mixin UseSprite on GameComponent {
  Sprite? sprite;

  @override
  void render(Canvas canvas) {
    super.render(canvas);
    if (this.isVisible) {
      sprite?.renderWithOpacity(
        canvas,
        position,
        size,
        opacity: opacity,
      );
    }
  }
}
